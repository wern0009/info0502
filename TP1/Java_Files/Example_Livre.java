public class Example_Livre {
    public static void main(String[] args) {
        // Step 1: Create Livre objects (Star Wars books)
        Livre livre1 = new Livre("Star Wars: Heir to the Empire", "HTTE123", 5, "Timothy Zahn", "9780553296129");
        Livre livre2 = new Livre("Star Wars: Thrawn", "THR456", 4, "Timothy Zahn", "9780345511270");

        // Step 2: Display the details of each book using the toString method
        System.out.println(livre1);
        System.out.println(livre2);

        // Step 3: Modify some attributes and display them again
        livre1.setAuteur("Tim Zahn");
        livre2.setNote(5);  // Change the note to 5

        // Display updated book details
        System.out.println("Updated details:");
        System.out.println(livre1);
        System.out.println(livre2);
    }
}

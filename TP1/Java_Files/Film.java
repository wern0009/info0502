public class Film extends Media {
    // Instance variables (private)
    private String realisateur;  // Director of the film
    private int annee;           // Release year of the film

    // Default constructor
    public Film() {
        super();  // Calls the default constructor of Media
        this.realisateur = "";
        this.annee = 0;
    }

    // Constructor with initialization
    public Film(String titre, String cote, int note, String realisateur, int annee) {
        super(titre, cote, note);  // Convert String to StringBuffer
        this.realisateur = realisateur;
        this.annee = annee;
    }


    // Copy constructor
    public Film(Film other) {
        super(other);  // Calls the copy constructor of Media
        this.realisateur = other.realisateur;
        this.annee = other.annee;
    }

    // Getters and Setters for Film class
    public String getRealisateur() {
        return this.realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public int getAnnee() {
        return this.annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    // Overriding the clone, equals, and toString methods
    @Override
    public Film clone() {
        return new Film(this);  // Calls the copy constructor of Film
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        
        Film film = (Film) obj;
        return this.annee == film.annee &&
               this.realisateur.equals(film.realisateur) &&
               super.equals(film);  // Compare Media properties as well
    }

    @Override
    public String toString() {
        return "Film{" +
               "titre='" + getTitre() + '\'' +
               ", cote='" + getCote() + '\'' +
               ", note=" + getNote() +
               ", realisateur='" + realisateur + '\'' +
               ", annee=" + annee +
               '}';
    }
}

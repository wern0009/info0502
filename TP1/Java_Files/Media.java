public class Media {
    // Instance variables (private)
    private String titre;           // Title of the media
    private StringBuffer cote;      // Code or rating of the media
    private int note;               // Rating assigned to the media

    // Class variable (private)
    private static String nom = "Media Library";  // Name of the media library, shared across all instances

    // Default constructor
    public Media() {
        this.titre = "";
        this.cote = new StringBuffer();
        this.note = 0;
    }

    // Constructor with initialization
    public Media(String titre, String cote, int note) {
        this.titre = titre;
        this.cote = new StringBuffer(cote);
        this.note = note;
    }

    // Copy constructor
    public Media(Media other) {
        this.titre = other.titre;
        this.cote = new StringBuffer(other.cote);
        this.note = other.note;
    }

    // Getters and Setters for instance variables
    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public StringBuffer getCote() {
        return new StringBuffer(this.cote);  // Returning a copy to avoid modification of the original object
    }

    public void setCote(String cote) {
        this.cote = new StringBuffer(cote);
    }

    public int getNote() {
        return this.note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // Class methods to access and modify the static variable 'nom'
    public static String getNom() {
        return nom;
    }

    public static void setNom(String newNom) {
        nom = newNom;
    }

    // Overriding clone, equals, and toString methods
    @Override
    public Media clone() {
        return new Media(this);  // Calls the copy constructor
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Media media = (Media) obj;
        return this.note == media.note && this.titre.equals(media.titre) && this.cote.toString().equals(media.cote.toString());
    }

    @Override
    public String toString() {
        return "Media{" + "titre='" + titre + '\'' + ", cote='" + cote + '\'' + ", note=" + note + '}';
    }
}

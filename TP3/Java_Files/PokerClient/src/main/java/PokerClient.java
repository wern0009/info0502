import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class PokerClient {
    private static final String SERVER_ADDRESS = "10.11.19.28"; // Update to your server's IP
    private static final int SERVER_PORT = 12345;

    public static void main(String[] args) {
        try (Socket connection = new Socket(SERVER_ADDRESS, SERVER_PORT);
             ObjectOutputStream output = new ObjectOutputStream(connection.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(connection.getInputStream())) {

            Scanner userInput = new Scanner(System.in);
            System.out.println("Connected to the Poker server on port " + SERVER_PORT);

            // Handle nickname input
            while (true) {
                String serverMessage = (String) input.readObject();
                System.out.println("Server: " + serverMessage);

                if (serverMessage.contains("Enter a unique nickname:")) {
                    System.out.print("Enter your username: ");
                    if (!userInput.hasNextLine()) {
                        System.out.println("Input stream is closed. Exiting...");
                        break;
                    }
                    String username = userInput.nextLine().trim();
                    output.writeObject(username);
                    output.flush();
                } else if (serverMessage.startsWith("Welcome")) {
                    break; // Exit nickname loop on success
                }
            }

            System.out.println("You are now connected! Type 'start' to begin or 'exit' to leave.");

            // Start a listener thread for server messages
            Thread serverListener = new Thread(() -> handleServerMessages(input));
            serverListener.start();

            // Handle user commands
            while (true) {
                if (!userInput.hasNextLine()) {
                    System.out.println("Input stream is closed. Exiting...");
                    break;
                }
                String command = userInput.nextLine().trim();
                output.writeObject(command);
                output.flush();

                if (command.equalsIgnoreCase("exit")) {
                    System.out.println("Exiting the game...");
                    break;
                }
            }

            serverListener.join(); // Wait for the listener thread to finish
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            System.err.println("Error occurred: " + e.getMessage());
        }
    }

    private static void handleServerMessages(ObjectInputStream input) {
        try {
            while (true) {
                Object serverMessage = input.readObject();
                if (serverMessage instanceof String) {
                    System.out.println("Server: " + serverMessage);
                } else {
                    System.out.println("Unrecognized data received from the server.");
                }
            }
        } catch (EOFException e) {
            System.out.println("Disconnected from server.");
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error while receiving messages: " + e.getMessage());
        }
    }
}

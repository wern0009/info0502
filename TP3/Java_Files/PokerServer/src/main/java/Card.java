import java.io.Serializable;

public class Card implements Serializable {
    private final int suit; // 0 = Hearts, 1 = Diamonds, 2 = Clubs, 3 = Spades
    private final int rank; // 0 = 2, 1 = 3, ..., 12 = Ace

    public Card(int suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public int getSuit() {
        return suit;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public String toString() {
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
        return ranks[rank] + " of " + suits[suit];
    }
}

import java.io.*;
import java.net.*;
import java.util.*;

public class PokerServer {
    private static final int PORT = 12345;
    private static final List<Player> players = new ArrayList<>();
    private static final int MIN_PLAYERS = 2;

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server started on port " + PORT);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New player connected.");
                new Thread(new PlayerHandler(clientSocket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class PlayerHandler implements Runnable {
        private final Socket clientSocket;
        private String nickname;
        private ObjectInputStream input;
        private ObjectOutputStream output;

        public PlayerHandler(Socket socket) {
            this.clientSocket = socket;
        }

        @Override
        public void run() {
            try {
                input = new ObjectInputStream(clientSocket.getInputStream());
                output = new ObjectOutputStream(clientSocket.getOutputStream());

                // Handle nickname selection
                while (true) {
                    output.writeObject("Enter a unique nickname:");
                    output.flush();

                    nickname = (String) input.readObject();
                    synchronized (players) {
                        if (players.stream().noneMatch(player -> player.getNickname().equals(nickname))) {
                            players.add(new Player(nickname, output));
                            output.writeObject("Welcome " + nickname + "!");
                            output.flush();
                            System.out.println(nickname + " has joined.");
                            break;
                        } else {
                            output.writeObject("Nickname already taken. Please choose another.");
                            output.flush();
                        }
                    }
                }

                // Keep the connection alive for commands
                while (true) {
                    String command = (String) input.readObject();
                    System.out.println(nickname + " sent command: " + command);

                    if ("exit".equalsIgnoreCase(command)) {
                        output.writeObject("Goodbye!");
                        break;
                    }

                    if ("start".equalsIgnoreCase(command)) {
                        handleStartCommand();
                    } else {
                        output.writeObject("Command received: " + command);
                    }

                    output.flush();
                }
            } catch (IOException | ClassNotFoundException e) {
                System.err.println("Error handling player connection: " + e.getMessage());
            } finally {
                disconnectPlayer();
            }
        }

        private void handleStartCommand() throws IOException {
            synchronized (players) {
                if (players.size() < MIN_PLAYERS) {
                    output.writeObject("Not enough players to start the game. Need at least " + MIN_PLAYERS + ".");
                    return;
                }

                // Notify all players about the game start
                for (Player player : players) {
                    player.getOutput().writeObject("Game is starting!");
                }

                // Basic game flow
                Deck deck = new Deck();
                deck.shuffle();

                // Deal two cards to each player
                for (Player player : players) {
                    Card[] hand = deck.deal(2);
                    player.setHand(hand);
                    player.getOutput().writeObject("Your hand: " + Arrays.toString(hand));
                }

                // Deal the community cards
                Card[] flop = deck.deal(3);
                Card turn = deck.deal(1)[0];
                Card river = deck.deal(1)[0];

                for (Player player : players) {
                    player.getOutput().writeObject("Flop: " + Arrays.toString(flop));
                    player.getOutput().writeObject("Turn: " + turn);
                    player.getOutput().writeObject("River: " + river);
                }

                // Determine winner
                Player winner = determineWinner(flop, turn, river);
                for (Player player : players) {
                    player.getOutput().writeObject("The winner is: " + winner.getNickname());
                }
            }
        }

        private Player determineWinner(Card[] flop, Card turn, Card river) {
            // Combine flop, turn, and river into a single array
            Card[] communityCards = new Card[5];
            System.arraycopy(flop, 0, communityCards, 0, 3);
            communityCards[3] = turn;
            communityCards[4] = river;

            Player bestPlayer = null;
            int bestHandRank = -1;

            for (Player player : players) {
                Card[] fullHand = new Card[7];
                System.arraycopy(player.getHand(), 0, fullHand, 0, 2);
                System.arraycopy(communityCards, 0, fullHand, 2, 5);

                int handRank = evaluateHand(fullHand); // Evaluate the player's hand
                if (handRank > bestHandRank) {
                    bestHandRank = handRank;
                    bestPlayer = player;
                }
            }

            return bestPlayer;
        }

        private int evaluateHand(Card[] hand) {
            // Simplified hand evaluation logic
            // This function should determine the rank of the best 5-card combination in the 7-card hand
            // Higher return values represent stronger hands
            return hand[0].getRank(); // Placeholder: Replace with actual hand ranking logic
        }

        private void disconnectPlayer() {
            synchronized (players) {
                if (nickname != null) {
                    players.removeIf(player -> player.getNickname().equals(nickname));
                    System.out.println(nickname + " has left the server.");
                }
            }
            try {
                clientSocket.close();
            } catch (IOException e) {
                System.err.println("Error closing client socket: " + e.getMessage());
            }
        }
    }

    static class Player {
        private final String nickname;
        private final ObjectOutputStream output;
        private Card[] hand;

        public Player(String nickname, ObjectOutputStream output) {
            this.nickname = nickname;
            this.output = output;
        }

        public String getNickname() {
            return nickname;
        }

        public ObjectOutputStream getOutput() {
            return output;
        }

        public void setHand(Card[] hand) {
            this.hand = hand;
        }

        public Card[] getHand() {
            return hand;
        }
    }
}

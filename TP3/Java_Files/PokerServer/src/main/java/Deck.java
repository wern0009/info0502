import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    private final List<Card> cards;

    public Deck() {
        cards = new ArrayList<>();
        for (int suit = 0; suit < 4; suit++) {
            for (int rank = 0; rank < 13; rank++) {
                cards.add(new Card(suit, rank));
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    public Card[] deal(int count) {
        Card[] dealtCards = new Card[count];
        for (int i = 0; i < count; i++) {
            dealtCards[i] = cards.remove(0);
        }
        return dealtCards;
    }
}

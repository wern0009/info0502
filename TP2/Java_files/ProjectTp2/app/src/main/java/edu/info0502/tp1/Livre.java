package edu.info0502.tp1;
public class Livre extends Media {
    // Instance variables specific to Livre
    private String auteur;    // Author of the book
    private String isbn;      // ISBN of the book

    // Default constructor
    public Livre() {
        super();  // Calls the default constructor of Media
        this.auteur = "";
        this.isbn = "";
    }

    // Constructor with initialization
    public Livre(String titre, String cote, int note, String auteur, String isbn) {
        super(titre, cote, note);  // Calls the parameterized constructor of Media
        this.auteur = auteur;
        this.isbn = isbn;
    }

    // Copy constructor
    public Livre(Livre other) {
        super(other);  // Calls the copy constructor of Media
        this.auteur = other.auteur;
        this.isbn = other.isbn;
    }

    // Getters and Setters for the Livre class
    public String getAuteur() {
        return this.auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    // Overriding toString method
    @Override
    public String toString() {
        return "Livre{" +
               "titre='" + getTitre() + '\'' +
               ", cote='" + getCote() + '\'' +
               ", note=" + getNote() +
               ", auteur='" + auteur + '\'' +
               ", isbn='" + isbn + '\'' +
               '}';
    }
}

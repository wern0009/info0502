package edu.info0502.tp1;
import java.util.Vector;
public class Mediatheque {
    // Instance variables
    private String proprietaire;   // Owner of the mediatheque
    private Vector<Media> medias;  // A collection (Vector) of Media objects (either Livre or Film)

    // Default constructor
    public Mediatheque() {
        this.proprietaire = "";
        this.medias = new Vector<>();
    }

    // Copy constructor
    public Mediatheque(Mediatheque other) {
        this.proprietaire = other.proprietaire;
        this.medias = new Vector<>(other.medias);  // Deep copy of the vector
    }

    // Method to add a media to the mediatheque
    public void add(Media media) {
        this.medias.add(media);
    }

    // Overriding the toString method to display details of the mediatheque
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Mediatheque owned by ").append(this.proprietaire).append("\n");
        sb.append("Media collection:\n");

        for (Media media : medias) {
            sb.append(media.toString()).append("\n");
        }
        return sb.toString();
    }

    // Getter and setter for proprietaire
    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }
}

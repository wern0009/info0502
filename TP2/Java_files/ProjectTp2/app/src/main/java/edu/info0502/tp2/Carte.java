package edu.info0502.tp2;

public class Carte {
    private String couleur;
    private String valeur;

    public Carte(String couleur, String valeur) {
        this.couleur = couleur;
        this.valeur = valeur;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getValeur() {
        return valeur;
    }

    @Override
    public String toString() {
        return valeur + " of " + couleur;
    }
}
package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Talon {
    private List<Carte> cartes;

    public Talon(int numberOfDecks) {
        cartes = new ArrayList<>();
        for (int i = 0; i < numberOfDecks; i++) {
            PaquetDeCartes paquet = new PaquetDeCartes();
            cartes.addAll(paquet.getCartes());
        }
        melanger();
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        return cartes.isEmpty() ? null : cartes.remove(0);
    }

    public void afficherCartes() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}

package edu.info0502.tp1;
public class Example_Mediatheque  {
    public static void main(String[] args) {
        // Step 1: Create media objects (Livre and Film) based on Star Wars
        Livre livre1 = new Livre("Star Wars: Heir to the Empire", "HTTE123", 5, "Timothy Zahn", "9780553296129");
        Livre livre2 = new Livre("Star Wars: Thrawn", "THR456", 4, "Timothy Zahn", "9780345511270");
        Film film1 = new Film("Star Wars: A New Hope", "SWANH1977", 10, "George Lucas", 1977);
        Film film2 = new Film("Star Wars: The Empire Strikes Back", "SWESB1980", 10, "Irvin Kershner", 1980);

        // Step 2: Create a Mediatheque and set its owner
        Mediatheque starWarsMediatheque = new Mediatheque();
        starWarsMediatheque.setProprietaire("Luke Skywalker");

        // Step 3: Add the media to the Mediatheque
        starWarsMediatheque.add(livre1);
        starWarsMediatheque.add(livre2);
        starWarsMediatheque.add(film1);
        starWarsMediatheque.add(film2);

        // Step 4: Display the contents of the Mediatheque
        System.out.println(starWarsMediatheque);
    }
}

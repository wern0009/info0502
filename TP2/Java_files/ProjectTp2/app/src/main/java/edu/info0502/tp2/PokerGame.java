package edu.info0502.tp2;

public class PokerGame {
    public static void main(String[] args) {
        // Create a talon (deck of cards) with 1 deck
        Talon talon = new Talon(1);

        // Simulate a poker game with 4 players
        Main[] players = new Main[4];

        // Deal hands to each player
        for (int i = 0; i < 4; i++) {
            players[i] = new Main(talon);
            System.out.println("Player " + (i + 1) + " hand:");
            players[i].afficherMain();
            System.out.println("Hand Evaluation: " + players[i].evaluerMain());
            System.out.println();
        }

        // Compare the hands (for now, we compare the first two players)
        System.out.println("Comparison between Player 1 and Player 2:");
        System.out.println(Main.comparerMains(players[0], players[1]));
    }
}

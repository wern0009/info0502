package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;

public class Main {
    private List<Carte> cartes;

    // Constructor for regular poker hand (5 cards)
    public Main(Talon talon) {
        cartes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            cartes.add(talon.tirerCarte());
        }
    }

    // Constructor for custom hand (e.g., for Texas Hold'em)
    public Main(List<Carte> cartes) {
        this.cartes = cartes;
    }

    // Getter to retrieve the list of cards in the hand
    public List<Carte> getCartes() {
        return cartes;
    }

    // Method to display the hand of cards
    public void afficherMain() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }

    // Method to evaluate the strength of a poker hand
    public String evaluerMain() {
        HashMap<String, Integer> valeurCount = new HashMap<>();
        HashMap<String, Integer> couleurCount = new HashMap<>();

        for (Carte carte : cartes) {
            String valeur = carte.getValeur();
            String couleur = carte.getCouleur();
            valeurCount.put(valeur, valeurCount.getOrDefault(valeur, 0) + 1);
            couleurCount.put(couleur, couleurCount.getOrDefault(couleur, 0) + 1);
        }

        boolean isFlush = couleurCount.containsValue(5);
        List<Integer> cardValues = getSortedCardValues();
        boolean isStraight = checkStraight(cardValues);

        if (isFlush && isStraight) {
            return "Straight Flush";
        } else if (valeurCount.containsValue(4)) {
            return "Four of a Kind";
        } else if (valeurCount.containsValue(3) && valeurCount.containsValue(2)) {
            return "Full House";
        } else if (isFlush) {
            return "Flush";
        } else if (isStraight) {
            return "Straight";
        } else if (valeurCount.containsValue(3)) {
            return "Three of a Kind";
        } else if (valeurCount.containsValue(2) && valeurCount.size() == 3) {
            return "Two Pair";
        } else if (valeurCount.containsValue(2)) {
            return "One Pair";
        } else {
            return "High Card";
        }
    }

    // Helper function to sort card values and check for straight
    private List<Integer> getSortedCardValues() {
        List<Integer> values = new ArrayList<>();
        for (Carte carte : cartes) {
            values.add(convertCardValueToInt(carte.getValeur()));
        }
        Collections.sort(values);
        return values;
    }

    // Helper function to check if card values form a straight
    private boolean checkStraight(List<Integer> values) {
        for (int i = 0; i < values.size() - 1; i++) {
            if (values.get(i) + 1 != values.get(i + 1)) {
                return false;
            }
        }
        return true;
    }

    // Convert card ranks to numerical values (Aces treated as both 1 and 14)
    private int convertCardValueToInt(String valeur) {
        switch (valeur) {
            case "2": return 2;
            case "3": return 3;
            case "4": return 4;
            case "5": return 5;
            case "6": return 6;
            case "7": return 7;
            case "8": return 8;
            case "9": return 9;
            case "10": return 10;
            case "Jack": return 11;
            case "Queen": return 12;
            case "King": return 13;
            case "Ace": return 14;
            default: return 0;
        }
    }

    // Method to compare two hands and determine the stronger hand
    public static String comparerMains(Main main1, Main main2) {
        List<String> handRanks = Arrays.asList(
            "High Card", "One Pair", "Two Pair", "Three of a Kind",
            "Straight", "Flush", "Full House", "Four of a Kind", "Straight Flush"
        );

        String evaluation1 = main1.evaluerMain();
        String evaluation2 = main2.evaluerMain();

        int rank1 = handRanks.indexOf(evaluation1);
        int rank2 = handRanks.indexOf(evaluation2);

        // First, compare the rank of each hand
        if (rank1 > rank2) {
            return "Player 1 wins with " + evaluation1;
        } else if (rank1 < rank2) {
            return "Player 2 wins with " + evaluation2;
        } else {
            // If both hands are of the same type (e.g., both are "High Card" or "One Pair"), compare card values
            List<Integer> hand1Values = main1.getSortedCardValues();
            List<Integer> hand2Values = main2.getSortedCardValues();

            // Compare the highest cards first
            for (int i = hand1Values.size() - 1; i >= 0; i--) {
                if (hand1Values.get(i) > hand2Values.get(i)) {
                    return "Player 1 wins with " + evaluation1;
                } else if (hand1Values.get(i) < hand2Values.get(i)) {
                    return "Player 2 wins with " + evaluation2;
                }
            }

            // If all cards are the same in value, it’s a tie
            return "It's a tie with " + evaluation1;
        }
    }

    // Main entry point for testing the poker hands with clear winner output
    public static void main(String[] args) {
        // Create a talon (deck of cards) with 1 deck
        Talon talon = new Talon(1);

        // Simulate a 4-player poker game
        Main[] players = new Main[4];
        int winningPlayer = 1; // To track the winning player
        Main strongestHand = null; // To track the strongest hand

        // Deal hands to each player and evaluate them
        for (int i = 0; i < 4; i++) {
            players[i] = new Main(talon);
            System.out.println("Player " + (i + 1) + " hand:");
            players[i].afficherMain();
            System.out.println("Hand Evaluation: " + players[i].evaluerMain());
            System.out.println();

            if (strongestHand == null || comparerMains(strongestHand, players[i]).contains("Player 2")) {
                strongestHand = players[i]; // Update strongest hand
                winningPlayer = i + 1; // Update winning player (1-indexed)
            }
        }

        // Display the winning player and their hand evaluation
        System.out.println("Player " + winningPlayer + " won with: " + strongestHand.evaluerMain());
    }
}

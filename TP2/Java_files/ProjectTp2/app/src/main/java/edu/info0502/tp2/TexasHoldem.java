package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.List;

public class TexasHoldem {
    private List<Carte> publicCards;

    public TexasHoldem(Talon talon) {
        publicCards = new ArrayList<>();

        // Deal 5 public community cards
        for (int i = 0; i < 5; i++) {
            publicCards.add(talon.tirerCarte());
        }
    }

    // Display public cards
    public void afficherPublicCards() {
        System.out.println("Public Cards:");
        for (Carte carte : publicCards) {
            System.out.println(carte);
        }
    }

    // Combine private and public cards and evaluate the best hand
    public String evaluerMain(Main privateCards) {
        // Combine the 5 public cards with the player's 2 private cards
        List<Carte> combinedHand = new ArrayList<>(privateCards.getCartes());
        combinedHand.addAll(publicCards);

        // Create a new Main object with the combined hand
        Main bestHand = new Main(combinedHand);

        // Evaluate the best hand
        return bestHand.evaluerMain();
    }

    public static void main(String[] args) {
        // Create a talon (deck of cards)
        Talon talon = new Talon(1);

        // Deal 2 private cards to each player
        Main player1Private = new Main(talon);
        Main player2Private = new Main(talon);

        // Create the Texas Hold'em game
        TexasHoldem game = new TexasHoldem(talon);

        // Display the public cards
        game.afficherPublicCards();

        // Evaluate each player's best hand using private + public cards
        System.out.println("\nPlayer 1's best hand:");
        System.out.println(game.evaluerMain(player1Private));

        System.out.println("\nPlayer 2's best hand:");
        System.out.println(game.evaluerMain(player2Private));
    }
}

package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PaquetDeCartes {
    private List<Carte> cartes;

    public PaquetDeCartes() {
        cartes = new ArrayList<>();
        String[] couleurs = {"Hearts", "Spades", "Diamonds", "Clubs"};
        String[] valeurs = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

        for (String couleur : couleurs) {
            for (String valeur : valeurs) {
                cartes.add(new Carte(couleur, valeur));
            }
        }
    }

    public void melanger() {
        Collections.shuffle(cartes);
    }

    public Carte tirerCarte() {
        return cartes.isEmpty() ? null : cartes.remove(0);
    }

    public List<Carte> getCartes() {
        return cartes;
    }

    public void afficherCartes() {
        for (Carte carte : cartes) {
            System.out.println(carte);
        }
    }
}

package qcmsystem;
import com.rabbitmq.client.*;
import java.nio.charset.StandardCharsets;

public class QCMClient {
    private static final String HOST = "localhost";

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            // Send registration
            String registerMessage = "{\"username\": \"john_doe\", \"password\": \"securepass\"}";
            sendRequest(channel, "register_queue", registerMessage);

            // Send authentication
            String authMessage = "{\"username\": \"john_doe\", \"password\": \"securepass\"}";
            sendRequest(channel, "auth_queue", authMessage);

            // Request QCM
            String qcmRequest = "{\"qcm_id\": \"12345\"}";
            sendRequest(channel, "qcm_request_queue", qcmRequest);

            // Submit answers
            String answers = "{\"answers\": {\"1\": \"4\", \"2\": \"15\"}}";
            sendRequest(channel, "answer_queue", answers);
        }
    }

    private static void sendRequest(Channel channel, String queue, String message) throws Exception {
        channel.queueDeclare(queue, true, false, false, null);
        channel.basicPublish("", queue, null, message.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Sent to " + queue + ": " + message);
    }
}

package qcmsystem;
import com.rabbitmq.client.*;
import com.google.gson.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class QCMServer {
    private static final String HOST = "localhost";
    private static final Map<String, String> userDatabase = new HashMap<>(); // Simulates user storage
    private static final Map<String, String> qcmAnswers = new HashMap<>(); // Stores answers for QCMs

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            // Initialize queues
            channel.queueDeclare("register_queue", true, false, false, null);
            channel.queueDeclare("auth_queue", true, false, false, null);
            channel.queueDeclare("qcm_request_queue", true, false, false, null);
            channel.queueDeclare("answer_queue", true, false, false, null);
            channel.queueDeclare("result_queue", true, false, false, null);

            System.out.println(" [*] Server is running. Waiting for messages...");

            // Registration handler
            channel.basicConsume("register_queue", true, (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] Register request received: " + message);

                JsonObject request = JsonParser.parseString(message).getAsJsonObject();
                String username = request.get("username").getAsString();
                String password = request.get("password").getAsString();

                if (userDatabase.containsKey(username)) {
                    sendResponse(channel, "{\"status\": \"failure\", \"message\": \"Username already exists\"}");
                } else {
                    userDatabase.put(username, password);
                    sendResponse(channel, "{\"status\": \"success\", \"message\": \"User registered\"}");
                }
            }, consumerTag -> {});

            // Authentication handler
            channel.basicConsume("auth_queue", true, (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] Auth request received: " + message);

                JsonObject request = JsonParser.parseString(message).getAsJsonObject();
                String username = request.get("username").getAsString();
                String password = request.get("password").getAsString();

                if (userDatabase.getOrDefault(username, "").equals(password)) {
                    sendResponse(channel, "{\"status\": \"success\", \"message\": \"Authentication successful\"}");
                } else {
                    sendResponse(channel, "{\"status\": \"failure\", \"message\": \"Invalid credentials\"}");
                }
            }, consumerTag -> {});

            // QCM request handler
            channel.basicConsume("qcm_request_queue", true, (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] QCM request received: " + message);

                String qcmJson = "{ \"qcm_id\": \"12345\", \"title\": \"Basic Math\", \"questions\": ["
                        + "{\"id\": \"1\", \"text\": \"What is 2+2?\", \"options\": [\"3\", \"4\", \"5\"]},"
                        + "{\"id\": \"2\", \"text\": \"What is 5*3?\", \"options\": [\"10\", \"15\", \"20\"]}"
                        + "]}";
                qcmAnswers.put("1", "4");
                qcmAnswers.put("2", "15");
                sendResponse(channel, qcmJson);
            }, consumerTag -> {});

            // Answer handler
            channel.basicConsume("answer_queue", true, (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [x] Answers received: " + message);

                JsonObject request = JsonParser.parseString(message).getAsJsonObject();
                JsonObject answers = request.getAsJsonObject("answers");

                int correct = 0, incorrect = 0;
                for (Map.Entry<String, String> entry : qcmAnswers.entrySet()) {
                    String questionId = entry.getKey();
                    String correctAnswer = entry.getValue();
                    if (answers.has(questionId) && answers.get(questionId).getAsString().equals(correctAnswer)) {
                        correct++;
                    } else {
                        incorrect++;
                    }
                }

                String result = String.format("{\"correct\": %d, \"incorrect\": %d}", correct, incorrect);
                sendResponse(channel, result);
            }, consumerTag -> {});

            // Keep the server running
            Object lock = new Object();
            synchronized (lock) {
                lock.wait();
            }
        }
    }

    private static void sendResponse(Channel channel, String message) throws Exception {
        channel.basicPublish("", "result_queue", null, message.getBytes(StandardCharsets.UTF_8));
        System.out.println(" [x] Sent response: " + message);
    }
}

package qcmsystem;

public class App {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: App <QCMServer|QCMClient>");
            return;
        }

        switch (args[0]) {
            case "QCMServer":
                try {
                    QCMServer.main(new String[0]);
                } catch (Exception e) {
                    System.err.println("Error running QCMServer: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case "QCMClient":
                try {
                    QCMClient.main(new String[0]);
                } catch (Exception e) {
                    System.err.println("Error running QCMClient: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Unknown argument. Use 'QCMServer' or 'QCMClient'.");
        }
    }
}

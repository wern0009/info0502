# Poker Game with MQTT (TP5)

## Overview
This project implements a distributed poker game using Mosquitto MQTT.

## Files
- `MQTTUtils.java`: Utility for MQTT connections.
- `PokerPublisher.java`: Publishes poker messages.
- `PokerSubscriber.java`: Subscribes to game topics.
- `PokerGameUtils.java`: Helper for JSON message generation.
- `PokerGameMaster.java`: Orchestrates the poker game.
- `TableManager.java`: Manages multiple poker tables.

## Setup
1. Install Mosquitto.
2. Configure `mosquitto.conf` and restart the service.
3. Compile the project using Gradle:
   ```bash
   gradle build
package poker;

import org.eclipse.paho.client.mqttv3.*;

public class PokerGameMaster {
    public static void main(String[] args) throws Exception {
        String brokerUrl = "tcp://localhost:1883";
        String topic = "poker/table1";

        MqttClient gameMaster = MQTTUtils.connect(brokerUrl, "PokerGameMaster");

        String startGameMessage = PokerGameUtils.createStartGameMessage(2);
        gameMaster.publish(topic, new MqttMessage(startGameMessage.getBytes()));
        System.out.println("Game started: " + startGameMessage);

        String dealCardsMessage = PokerGameUtils.createCardDistributionMessage("player1", "Ace of Spades", "10 of Hearts");
        gameMaster.publish(topic, new MqttMessage(dealCardsMessage.getBytes()));
        System.out.println("Cards dealt: " + dealCardsMessage);

        gameMaster.disconnect();
    }
}
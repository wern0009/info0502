package poker;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class PokerPublisher {
    public static void main(String[] args) throws Exception {
        String brokerUrl = "tcp://localhost:1883";
        String topic = "poker/table1";

        MqttClient publisher = MQTTUtils.connect(brokerUrl, "PokerPublisher");
        String message = "{\"action\":\"start_game\",\"players\":2}";
        publisher.publish(topic, new MqttMessage(message.getBytes()));
        System.out.println("Message published: " + message);

        publisher.disconnect();
    }
}
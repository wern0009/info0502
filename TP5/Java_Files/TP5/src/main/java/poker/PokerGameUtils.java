package poker;

import org.json.JSONObject;

public class PokerGameUtils {
    public static String createStartGameMessage(int playerCount) {
        JSONObject json = new JSONObject();
        json.put("action", "start_game");
        json.put("players", playerCount);
        return json.toString();
    }

    public static String createCardDistributionMessage(String player, String card1, String card2) {
        JSONObject json = new JSONObject();
        json.put("action", "deal_cards");
        json.put("player", player);
        json.put("card1", card1);
        json.put("card2", card2);
        return json.toString();
    }
}
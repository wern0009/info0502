package poker;

import org.eclipse.paho.client.mqttv3.*;

public class TableManager {
    public static void main(String[] args) throws Exception {
        String brokerUrl = "tcp://localhost:1883";
        String adminTopic = "poker/admin";

        MqttClient adminClient = MQTTUtils.connect(brokerUrl, "TableManager");

        String createTableMessage = "{\"action\":\"create_table\",\"table\":\"table2\"}";
        adminClient.publish(adminTopic, new MqttMessage(createTableMessage.getBytes()));
        System.out.println("Table created: " + createTableMessage);

        String closeTableMessage = "{\"action\":\"close_table\",\"table\":\"table2\"}";
        adminClient.publish(adminTopic, new MqttMessage(closeTableMessage.getBytes()));
        System.out.println("Table closed: " + closeTableMessage);

        adminClient.disconnect();
    }
}
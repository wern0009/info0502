package poker;

import org.eclipse.paho.client.mqttv3.*;

public class PokerSubscriber {
    public static void main(String[] args) throws Exception {
        String brokerUrl = "tcp://localhost:1883";
        String topic = "poker/#";

        MqttClient subscriber = MQTTUtils.connect(brokerUrl, "PokerSubscriber");

        subscriber.subscribe(topic, (t, message) -> {
            String payload = new String(message.getPayload());
            System.out.println("Message received: Topic: " + t + ", Payload: " + payload);
        });

        System.out.println("Subscriber is listening on topic: " + topic);
        while (true) {
        }
    }
}
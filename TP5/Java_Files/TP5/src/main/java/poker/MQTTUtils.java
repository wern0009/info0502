package poker;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class MQTTUtils {
    public static MqttClient connect(String brokerUrl, String clientId) {
        try {
            MqttClient client = new MqttClient(brokerUrl, clientId);
            client.connect();
            return client;
        } catch (MqttException e) {
            throw new RuntimeException("Failed to connect to broker: " + brokerUrl, e);
        }
    }
}
package poker;

import org.json.JSONObject;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class PokerGameTest {

    @Test
    public void testGeneratedMessages() throws Exception {
        String testData = new String(Files.readAllBytes(Paths.get("src/test/resources/test-data.json")));
        JSONObject testCases = new JSONObject(testData);
        JSONArray cases = testCases.getJSONArray("test_cases");

        for (int i = 0; i < cases.length(); i++) {
            JSONObject testCase = cases.getJSONObject(i);
            String description = testCase.getString("description");
            JSONObject expected = testCase.getJSONObject("expected");
            JSONObject input = testCase.getJSONObject("input");

            switch (expected.getString("action")) {
                case "start_game":
                    String startGameMessage = PokerGameUtils.createStartGameMessage(input.getInt("playerCount"));
                    JSONObject actualStartGame = new JSONObject(startGameMessage);
                    assertEquals(expected.toString(), actualStartGame.toString(), description);
                    break;

                case "deal_cards":
                    String dealCardsMessage = PokerGameUtils.createCardDistributionMessage(
                            input.getString("player"), input.getString("card1"), input.getString("card2"));
                    JSONObject actualDealCards = new JSONObject(dealCardsMessage);
                    assertEquals(expected.toString(), actualDealCards.toString(), description);
                    break;

                case "create_table":
                case "close_table":
                    JSONObject actualTableAction = new JSONObject(input.toString());
                    assertEquals(expected.toString(), actualTableAction.toString(), description);
                    break;

                default:
                    fail("Unknown action: " + expected.getString("action"));
            }
        }
    }
}